var way1 = "hall/6/bill/";
var way2 = "hall/6/table/";
var way3 = "hall/6/table";
var fs = require('fs');
var path = require('path');
$('#leftbar,#workspace,#mark').css('height', $(window).height() - 41);
$('#workspace').css('width', ($(window).width() - $('#leftbar').width() + 1));
setTimeout(function() {
    $('#leftbar').trigger('mouseover');
}, 100);
$('#leftbar').on('mouseover', function() {
    $('#workspace').css('width', $(window).width() - ($('#leftbar').width() + 1));
});
$(window).resize(function() {
    $('#leftbar,#workspace,#mark').css('height', $(window).height() - 41);
    $('#workspace').css('width', $(window).width() - ($('#leftbar').width() + 1));
});
$('#mark').on('mousedown', function(e) {
    $(window).on('mousemove', function(e) {
        $('#leftbar').css('width', e.pageX);
    });
});
$(window).on('mouseup', function(e) {
    $(window).unbind("mousemove");
});
$('select').on('change', function() {
    $(this).parent().css('background-color', $(this).val());
});
var itable;
$('.table button').on('click', function() {
     itable = $('#workspace .table').length;
    var bgcolor = $(this).parent();
    var bgcolor = $(bgcolor).find('select').val();
    if (bgcolor == 'Black' || bgcolor == 'Green' || bgcolor == 'Purple' || bgcolor == 'Blue' || bgcolor == 'Brown') {
        $(this).parent().css('color', 'white');
    } else if (bgcolor == 'Pink' || bgcolor == "Skyblue" || bgcolor == "Yellow" || bgcolor == "Red" || bgcolor == "Orange" || bgcolor == "White") {
        $(this).parent().css('color', 'black');
    } else {
        $(this).parent().css('color', 'white');
    }
    var thisdiv = $(this).parent().clone();
    $(thisdiv).empty();
    $(thisdiv).append('<span class="close" onclick="tableclose()">Close</span>');
    $(thisdiv).append('<br><span class="index">' + itable + '</span>');
    $(thisdiv).append('<br><button class="bill" onclick="billopen($(this).parent().find(\'.index\').text())">Bill</button>');
    itable++;
    $(thisdiv).prependTo('#workspace');
    move();
});

function move() {
    $('#workspace .table').on('mousedown', function() {
        var item = $(this);
        $('#workspace').on('mousemove', function(e) {
            $(item).css('top', e.pageY - $('#menu').height() - 30);
            $(item).css('left', e.pageX - $('#leftbar').width() - 50);
            $('#workspace').on('mouseup', function(e) {
                $('#workspace').unbind("mousemove");
            });
        });
    });
}
move();
$('.table').on('click', function(e) {
    $('#workspace .table').css('position', 'absolute');
});
//-----------------------------------------
function tableclose() {
        $('#workspace .close').on('click', function() {
            $(this).parent().remove();
            itable = $('#workspace .table').length;
            if(itable){
            for (var i = 0; i !== itable; i++) {
                $('#workspace .table').eq(i).find('.index').text(i);
            }
            }
        });
    }
    //-----------------------------------------
function hold() {
        if ($('#hold:checked').length) {
            $('.close').hide();
            $('.addbtn').hide();
            $('#workspace .table').unbind('mousedown');
        } else {
            $('.close').show();
            $('.addbtn').show();
            $('#workspace .table').bind('mousedown', move());
        }
    }
    //-----------------------------------------
function billopen(index) {
     $('span[index=' + index + ']').parent().css('box-shadow', '0 0 10px 6px darkred');
        var html = $('#billtemp').html();
        var billdata;
        var stats;
        try {
            stats = fs.lstatSync(way1 + index + '.json');
            if (stats.isFile()) {
                billdata = fs.readFileSync(way1 + index + '.json');
                billdata = JSON.parse(billdata);
                var temp = Handlebars.compile(html);
                $('#main').append(temp(billdata));
            }
        } catch (e) {
            var temp = Handlebars.compile(html);
            $('#main').append(temp());
        }
        $('.tableindex span').text(index);
        $('.table .bill').hide();
        $('.table button').hide();
        (function() {
            $('.billform').on('mousedown', function() {
                var item = $(this);
                $(window).on('mousemove', function(e) {
                    $(item).css('top', e.pageY - 300);
                    $(item).css('left', e.pageX - 600);
                    $(window).on('mouseup', function(e) {
                        $(window).unbind("mousemove");
                    });
                });
            });
        })();
        $('.formclose').click(function() {
         window.location.reload();
        });
        $('.formprint').click(function() {
                $(window).unbind("mousemove");
            $('#menu').remove();
            $('#leftbar').remove();
            $('#workspace .table').remove();
            $('#workspace').css({width: screen.width, height:screen.height, backgroundColor: "white"});
                  $('.billform .addbar').remove();
                    window.print();                  
        });
        calc();
        checkit(".price", /[a-zA-Zа-яА-Я]+/, "red", "blue");
        checkit(".count", /[a-zA-Zа-яА-Я]+/, "red", "blue");
        if ($('.lookbar input').length > 3) {
            $('span[index=' + index + ']').parent().css('box-shadow', '0 0 10px 6px red');
        } 
           
       
    inputcomplete();
    }
    //-----------------------------------------
function addsave(index) {
        if ($('.price').length < 1) {
            try {
                fs.unlinkSync(way1 + index + '.json');
            } catch (e) {}
            return true;
        }
        var billtext = ' ';
        for (var i = 0; i !== $('.food').length; i++) {
            var food = $('.food').eq(i).val();
            var price = $('.price').eq(i).val();
            var count = $('.count').eq(i).val();
            food = food.replace(/"/g, "");
            food = food.replace(/'/g, "");
            food = food.replace(/ /g, "_");
            billtext += '"' + i + '": {"food": "' + food + '","price": "' + price + '","count": "' + count + '"}';
            if ($('.food').length > 1 && i + 1 !== $('.food').length) {
                billtext += ',\n';
            }
        }
        try {
            fs.unlinkSync(way1 + index + '.json');
        } catch (e) {
            console.log('ok');
        }
        fs.writeFileSync(way1 + index + '.json', '{\n"billmass":{' + billtext + '}}');
    }
    //-----------------------------------------
function clearinput() {
        $('.billform .addbar .price').val('');
        $('.billform .addbar .food').val('');
        $('.billform .addbar .count').val('');
        $('.billform .addbar .curfield').remove();
        console.log('All input ared clear!')
    }
    //-----------------------------------------
function startdraw(){
fs.readdir(way3, function(err, list) { if(!err){draw(list); var i=0; i++; console.log(i);}});
function draw(list){
    for (var i = 0; i !==list.length; i++) {
        var data =false;
        try{
      data = fs.readFileSync(way2 + i + '.json', "utf8");
      
        if (data !==false) {
            var tableinfo = JSON.parse(data);
            var clonewhat = '#leftbar .table[type=' + tableinfo.type + ']';
            var itdiv = $(clonewhat).clone();
            $(itdiv).empty();
            $(itdiv).append('<span class="close" onclick="tableclose()">Close</span>');
            $(itdiv).append('<br><span class="index" index="' + i + '">' + i + '</span>');
            $(itdiv).append('<br><button class="bill" onclick="billopen($(this).parent().find(\'.index\').text())">Bill</button>');
            $(itdiv).css('position', 'absolute');
            $(itdiv).css('top', tableinfo.top);
            $(itdiv).css('left', tableinfo.left);
            $(itdiv).css('color', tableinfo.color);
            $(itdiv).css('background-color', tableinfo.bgcolor);
     $(itdiv).prependTo('#workspace');
            hold();
            active();
        delete itdiv;
            delete tableinfo;
            delete clonewhat;
        }
              }
        catch(e){
        data =false;
            
        
        }
    }
}
}
startdraw();
$('#hold').prop({
    checked: true
});
$('.close').hide();
$('.addbtn').hide();
//------------------------------
function savepos() {
        try {
            fs.readdir(way3, function(err, list) {
                for (var i = 0; i !== list.length - 1; i++) {
                    stats = fs.lstatSync(way2 + i + '.json');
                    if (stats.isFile()) {
                        fs.unlinkSync(way2 + i + '.json');
                    }
                }
                for (var i = 0; i !== $('#workspace .table').length; i++) {
                    if (!$('#workspace .table').length) break;
                    if (i > 200) break;
                    var top = $('#workspace .table').eq(i).css("top");
                    var left = $('#workspace .table').eq(i).css("left");
                    var bgcolor = $('#workspace .table').eq(i).css("background-color");
                    var color = $('#workspace .table').eq(i).css("color");
                    var type = $('#workspace .table').eq(i).attr('type');
                    fs.writeFileSync(way2 + i + '.json', '{"top":"' + top + '",\n"left":"' + left + '",\n"type":"' + type + '",\n"color":"' + color + '",\n"bgcolor":"' + bgcolor + '"}');
                }
            });
            console.log('Table position is save!')
        } catch (e) {
            console.log('no "table" dir')
        }
    }
    //------------------------------
var inputid = 0;

function addinput() {
    inputid++;
    $('.billform .addbar .fornewinput').append('<span number="' + inputid + '"><br><button onclick="$(\'span[number=' + inputid + ']\').remove();">-</button>' + inputid + '<br><input type="text" class="food" placeholder="name"><input type="text" class="price" placeholder="price"><input type="number" class="count" placeholder="count"></span>');
    checkit(".price", /[a-zA-Zа-яА-Я]+/, "red", "blue");
    checkit(".count", /[a-zA-Zа-яА-Я]+/, "red", "blue");
    inputcomplete();
}

function delallinput() {
    $('.addbar .fornewinput span').remove();
    $('.billform .addbar .curfield').remove();
    inputid = 0;
    console.log('Input deleted!')
}

function calc() {
    var total = 0;
    for (var i = 0; i !== $('.billform .lookbar .curprice').length; i++) {
        total += $('.billform .lookbar .curprice').eq(i).val() * $('.billform .lookbar .curcount').eq(i).val();
        $('.billform .total').text(total);
    }
    console.log('Total is ok!')
}

function checkit(elem, reg, color1, color2) {
    $(elem).on('change', function() {
        if ($(this).val().search(reg) != -1) {
            $(this).css('border-color', color1);
            $(this).val('');
            $(this).attr('placeholder', 'Only numbers!');
        } else if ($(this).val().length == 0) {
            $(this).css('border-color', color1);
            $(this).val('');
            $(this).attr('placeholder', 'Empty!');
        } else {
            $(this).css('border-color', color2);
        }
    });
}
checkit(".price", /[a-zA-Zа-яА-Я]+/, "red", "blue");
checkit(".count", /[a-zA-Zа-яА-Я]+/, "red", "blue");

function active() {
    if ($('#workspace .table').length > 0) {
        for (var i = 0; i !== $('#workspace .table').length; i++) {
            try {
                var billindex = fs.lstatSync(way1 + i + '.json');
                if (billindex.isFile()) {
                    var spanindex = 'span[index=' + i + ']';
                    $(spanindex).parent().css('border', '4px dashed');
                }
            } catch (e) {
                console.log('Not active!');
            }
        }
    }
};

 function inputcomplete() {
         var availableTags = ["Coffee", "Cola", "Burger", "Milk", "Beer", "Cake", "Water"];
     try {
    availableTags =  fs.readFileSync("products/list.txt");
         availableTags = availableTags.toString().split(',');
     }
     catch(e){
         
         alert("Can't found 'list.txt' !")
         
     }
    $( ".food" ).autocomplete({
      source: availableTags
    });
  };

function addDate(){
    setInterval(function(){
var date = new Date();
var hours = date.getHours();
var minutes = date.getMinutes();
var seconds = date.getSeconds();
    $('title').text(hours+':'+minutes+':'+seconds);
    }, 1000);
}
addDate();
